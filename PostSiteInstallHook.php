<?php

namespace Drush\Commands\drush_si_file_permissions;

use Consolidation\AnnotatedCommand\CommandData;
use DrupalFinder\DrupalFinder;
use Drush\Commands\DrushCommands;

/**
 * Drush 9 and 10 hooks.
 */
class PostSiteInstallHook extends DrushCommands
{
    /**
     * Make the defaults folder writable.
     *
     * @hook post-command site:install
     */
    public function afterSiteInstall($result, CommandData $commandData)
    {
        $drupalFinder = new DrupalFinder();
        // Make the folder and files writable again after the drupal site install
        // makes them read only for security reasons. But we install the site only
        // in development and CI. So this is unnecessary.
        if ($drupalFinder->locateRoot($this->getConfig()->cwd())) {
            $drupalRoot = $drupalFinder->getDrupalRoot();
            // Maybe one day we can make it configurable which folder and what
            // permissions to set. But for now this solves the default case.
            chmod($drupalRoot.'/sites/default', 0755);
            chmod($drupalRoot.'/sites/default/settings.php', 0644);
            if ($this->io()->isVerbose()) {
                $this->io()->success("Changed folder permission of $drupalRoot/sites/default");
            }
        } else {
            $this->io()->error('Could not find drupal root!');
        }

        return $result;
    }
}
