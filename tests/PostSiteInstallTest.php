<?php

namespace NuvoleDrushExtension;

use PHPUnit\Framework\TestCase;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test the drush command hook.
 */
class PostSiteInstallTest  extends TestCase
{
    use DrushTestTrait;

    protected const EXTENSION_DIR = 'sut/drush/Commands/drush-si-file-permissions/';
    protected const MESSAGE_PATTERN = 'Changed folder permission of';

    /**
     * Test to check the file permissions after a site install.
     *
     * @covers PostSiteInstallHook::afterSiteInstall
     */
    public function testSiteInstall()
    {
        static::installExtension();

        $this->drush('site-install', ['--yes'], ['db-url' => 'sqlite://sites/default/files/.ht.sqlite']);

        static::assertStringContainsString('[success] Installation complete.', $this->getErrorOutputRaw());
        static::assertStringNotContainsString(static::MESSAGE_PATTERN, $this->getOutputRaw());

        static::assertEquals(0755, fileperms('sut/web/sites/default/') & 0777);
        static::assertEquals(0644, fileperms('sut/web/sites/default/settings.php') & 0777);
    }

    /**
     * Test to check the file permissions after a site install.
     *
     * @covers PostSiteInstallHook::afterSiteInstall
     */
    public function testVerboseSiteInstall()
    {
        static::installExtension();

        $this->drush('site-install', ['--yes', '-v'], ['db-url' => 'sqlite://sites/default/files/.ht.sqlite']);

        static::assertStringContainsString('[success] Installation complete.', $this->getErrorOutputRaw());
        static::assertStringContainsString(static::MESSAGE_PATTERN, $this->getOutputRaw());

        static::assertEquals(0755, fileperms('sut/web/sites/default/') & 0777);
        static::assertEquals(0644, fileperms('sut/web/sites/default/settings.php') & 0777);
    }

    /**
     * Test to check the file permissions after a install without the extension.
     */
    public function testVanillaSiteInstall()
    {
        $this->drush('site-install', ['--yes', '-v'], ['db-url' => 'sqlite://sites/default/files/.ht.sqlite']);

        static::assertStringContainsString('[success] Installation complete.', $this->getErrorOutputRaw());
        static::assertStringNotContainsString(static::MESSAGE_PATTERN, $this->getOutputRaw());

        static::assertEquals(0555, fileperms('sut/web/sites/default/') & 0777);
        static::assertEquals(0444, fileperms('sut/web/sites/default/settings.php') & 0777);
    }

    /**
     * Clean up the drupal installation.
     */
    public function tearDown(): void
    {
        // Remove files created by drupal site install.
        chmod('sut/web/sites/default/', 0755);
        unlink('sut/web/sites/default/settings.php');

        static::rrmdir('sut/web/sites/default/files');
        static::rrmdir(self::EXTENSION_DIR);
    }

    /**
     * Recursively remove a directory and all its contents.
     *
     * @param string $dir
     */
    protected static function rrmdir(string $dir): void
    {
        if (!is_dir($dir)) {
            return;
        }
        // Recursively delete all files and folders.
        $files = new \RecursiveIteratorIterator(
          new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
          \RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $fileinfo) {
            if ($fileinfo->isDir()) {
                rmdir($fileinfo->getRealPath());
            } else {
                unlink($fileinfo->getRealPath());
            }
        }
        rmdir($dir);
    }

    /**
     * Create a copy of the drush hook in the site under test.
     */
    protected static function installExtension(): void
    {
        // Simulate composer having installed the drush extension for the site
        // under test: "sut/drush/Commands/{$name}": ["type:drupal-drush"]
        mkdir(self::EXTENSION_DIR,0777, TRUE);
        copy('PostSiteInstallHook.php', self::EXTENSION_DIR . 'PostSiteInstallHook.php');
    }
}
