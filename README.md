# Drush site install file permissions

Drupal changes the file permissions to read only on `sites/default` and `sites/default/settings.php`
this is a good practice for the security of the site.
However, during development these restrictions often hinder us.

This drush extension adds a hook to post site-install to set the permissions to writable again.
